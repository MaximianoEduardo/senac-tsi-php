<?php
    $Aluno[0]["Nome:"] = "Lorem";
    $Aluno[0]["Bitbucket"] = "https://bitbucket...";
    $Aluno[1]["Nome:"] = "Ipsum";
    $Aluno[1]["Bitbucket"] = "https://bitbucket...";
    $Aluno[2]["Nome:"] = "CTZ";
    $Aluno[2]["Bitbucket"] = "https://bitbucket...";
    $Aluno[3]["Nome:"] = "123456";
    $Aluno[3]["Bitbucket"] = "https://bitbucket...";
    $Aluno[4]["Nome:"] = "159789";
    $Aluno[4]["Bitbucket"] = "https://bitbucket...";
    $Aluno[5]["Nome:"] = "qwwer";
    $Aluno[5]["Bitbucket"] = "https://bitbucket...";

    $i = 0;
    ?>
    <table>
      <thead>
        <th>Nome</th>
        <th>Bitbucket</th>
      </thead>
      <tbody>
        <?php
        while ($i < count($Aluno))
        {
          $nome_aluno = $Aluno[$i]['nome'];
          $bitbucket_link = $Aluno[$i]['bitbucket'];
          ?>
          <tr>
            <td><?php echo $nome_aluno; ?></td>
            <td><?php echo $bitbucket_link; ?></td>
          </tr>
          <? $i++;
        }
         ?>
      </tbody>
    </table>
