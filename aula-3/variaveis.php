<?php

// Constante no PHP

define('Qtd_pag', 10);

echo 'Valor da minha constante é :' . Qtd_pag .'<br>';

// variavel para passar valor para uma constante

$ip_do_banco ='10.0.0.1';

define('IP_DO_BANCO', $ip_do_banco);

echo 'O IP do banco é : ' . IP_DO_BANCO . '<br>';

// Variaveis Magicas
echo "Estou na linha " . __LINE__. '<br>';
echo "Este é o arquivo " . __FILE__. '<br>';

echo '<br>';
// Depurar o Codigo

var_dump($ip_do_banco);
echo '<br>';
echo '<br>';
// Vetor

$dias = ['seg','ter','qua','qui','sex','sab','dom'];

var_dump($dias);
echo '<br>';
echo '<br>';

$diasdasemana [0] = 'seg';
$diasdasemana [1] = 'ter';
$diasdasemana [2] = 'qua';
$diasdasemana [3] = 'qui';
$diasdasemana [4] = 'sex';
$diasdasemana [5] ='sab';
$diasdasemana [6] ='dom';

var_dump($diasdasemana);
echo '<br>';
echo '<br>';

$diasdasemana2 = array(0 => 'seg',1 => 'ter',2 => 'qua',3 => 'qui',4 => 'sex',5 => 'sab',6 => 'dom',);
var_dump($diasdasemana2);



 ?>
